var gulp = require('gulp');
var sass = require('gulp-sass');
var runSequence = require('gulp4-run-sequence');
var rename      = require('gulp-rename');
var buffer = require('gulp-buffer');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var wrap = require('gulp-wrap');
var path = require('path');
var autoprefixer = require('gulp-autoprefixer');

// Sass
gulp.task('sass', function () {
  gulp.src('src/assets/stylesheets/*.scss')
    .pipe(sass())
    .pipe(autoprefixer({
      cascade: false
    }))
    .pipe(gulp.dest('./public/assets/stylesheets'));
  return new Promise(function(resolve, reject) {
    resolve();
  });
});

// Concat all JS-plugins
gulp.task('concatJs', function() {
	return gulp.src(['./src/assets/scripts/*'])
	.pipe(wrap('\n//<%= file.path %>\n//-->\n<%= contents %>'))
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('./public/assets/scripts/'));
});

// Copy JS
gulp.task('copyJs', function() {
    gulp.src(['./src/assets/scripts/vendor/jquery/*'])
    .pipe(gulp.dest('./public/assets/scripts/jquery/'));
    return new Promise(function(resolve, reject) {
      resolve();
    });
});

// Copy Images
gulp.task('copyImages', function() {
    gulp.src('./src/resources/images/**/*')
    .pipe(gulp.dest('./public/assets/images/'));
    return new Promise(function(resolve, reject) {
      resolve();
    });
});

// Copy Html
gulp.task('copyHtml', function() {
  gulp.src('./src/*.html')
  .pipe(gulp.dest('./public/'));
  return new Promise(function(resolve, reject) {
    resolve();
  });
});

// Copy temporary pictures
gulp.task('copyTempPics', function() {
    gulp.src('./src/resources/temp/**/*')
    .pipe(gulp.dest('./public/temp/'));
    return new Promise(function(resolve, reject) {
      resolve();
    });
});

/// Delete the contents of all first level folders of './public' folder
// except html-files

gulp.task('clean', function () {
    return gulp.src(['./public/assets/*', './public/temp/*', ], {read: false})
    .pipe(clean({force: true}));
});


// Watch taskes
gulp.task('watch', function() {
  gulp.watch(['src/assets/stylesheets/*.scss',
            'src/assets/stylesheets/**/*.scss',
            'src/assets/stylesheets/**/**/*.scss',
            'src/templates/**/*.scss'], gulp.series('sass'));
  gulp.watch(['src/assets/scripts/*.*'], gulp.series('concatJs')); //
  gulp.watch(['./src/assets/scripts/vendor/jquery/*.*'], gulp.series('copyJs')); //
  gulp.watch(['src/*.html'], gulp.series('copyHtml'));
  gulp.watch(['src/resources/images/*.*', 'src/resources/images/**/*.*'], gulp.series('copyImages'));
  gulp.watch(['src/resources/temp/*.*', 'src/resources/temp/**/*.*'], gulp.series('copyTempPics'));
});

gulp.task('default', function(callback) {
  runSequence( 'clean',
              'sass',
              ['copyHtml',
               'concatJs',
               'copyJs',
               'copyImages',
               'copyTempPics'],
              'watch',
               callback);
});
